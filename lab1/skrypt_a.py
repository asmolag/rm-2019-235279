import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random

delta = 0.1
x_size = 10
y_size = 10
# obst_vect = [(1, 2), (1.5, 2.3), (0, 1), (1, 0)]
# start_point = (-10, -3)
# finish_point = (10, 3)
obst_vect = [(random.uniform(-10, 10), random.uniform(-10, 10)), (random.uniform(-10, 10), random.uniform(-10, 10)),
             (random.uniform(-10, 10), random.uniform(-10, 10)), (random.uniform(-10, 10), random.uniform(-10, 10))]
start_point = (-10, random.uniform(-10, 10))
finish_point = (10, random.uniform(-10, 10))

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)


kp = 1
koi = 10
d0 = 20

# qr    wspolrzedne polozenia robota
# qk    wspolrzedne celu
# pk    wspolczynnik dobierany doswiadczalnie
# qoi   wspolrzedne polozenia i-tej przeszkody
# koi   wspolczynnik dla kazdej przeszkody dobierany doswiadczalnie
# d0    odleglosc graniczna oddzialywania przeszkody


# potencjal przyciagajacy
def Up(qr, qk, kp):
    result = 1/2 * kp * np.linalg.norm(np.subtract(qr, qk))**2
    return result


# potencjal odpychajacy od i-tej przeszkody
def Voi(koi, qr, qoi, d0):
    condition = np.linalg.norm(np.subtract(qr, qoi))
    if condition <= d0:
        result = 1/2 * koi * ((1/condition) - (1/d0))**2
        return result
    else:
        return 0


# potencjal wypadkowy
def Uw(qr, qk, kp, koi, d0, obstacles):
    return Up(qr, qk, kp) + sum([Voi(koi, qr, qoi, d0) for qoi in obstacles])


# sila?
def Fp(qr, qk, kp):
    result = kp * np.linalg.norm(np.subtract(qr, qk))
    return result


def Foi(koi, qr, qoi, d0):
    condition = np.linalg.norm(np.subtract(qr, qoi))
    if condition <= d0 and condition!=0:
        result = -koi * ((1/condition)-(1/d0)) * (1 / condition**2)
        return result
    else:
        return 0


def F(qr, qk, kp, koi, d0, obstacles):
    return Fp(qr, qk, kp) - sum([Foi(koi, qr, qoi, d0) for qoi in obstacles])



# print('start_point: {}, finish_point: {}, kp: {}'.format(
#     start_point, finish_point, kp))
# print('obstacles: {}'.format(obst_vect))
# print('Up: {}'.format(Up(start_point, finish_point, kp)))
# print('Voi: {}'.format(Voi(koi, start_point, obst_vect[0], d0)))
# print('Voi_sum: {}'.format(
#     sum([Voi(koi, start_point, qoi, d0) for qoi in obst_vect])))
# print('Uw: {}'.format(Uw(start_point, finish_point, kp, koi, d0, obst_vect)))
# print('Fp: {}'.format(Fp(start_point, finish_point, kp)))
# print('Foi: {}'.format(Foi(koi, start_point, obst_vect[0], d0)))
# print('Foi_sum: {}'.format(
#     sum([Foi(koi, start_point, qoi, d0) for qoi in obst_vect])))
# print('F: {}'.format(F(start_point, finish_point, kp, koi, d0, obst_vect)))

# Z = []
for i in range(len(x)):
    for j in range(len(y)):
        # print(_x,_y)
        Z[j][i] = F((x[i], y[j]), finish_point, kp, koi, d0, obst_vect)
        if Z[j][i] < -1:
            Z[j][i] = -1

# macierz sil
#Zz = ( F((_x, _y), finish_point, kp, koi, d0, obst_vect) for _x,_y in zip(x,y) )
# print(np.asarray(list(Zz)))
# print(Z)

path = []
index_x = index_y = 0


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],vmax=20,vmin=0)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
