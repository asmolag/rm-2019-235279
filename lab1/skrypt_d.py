from matplotlib.animation import FuncAnimation
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import random
import time

delta = 0.1
x_size = 10
y_size = 10
# obst_vect = [(1, 2), (1.5, 2.3), (0, 1), (1, 0)]
# start_point = (-10, -3)
# finish_point = (10, 3)
obst_vect = [(random.uniform(-10, 10), random.uniform(-10, 10)), (random.uniform(-10, 10), random.uniform(-10, 10)),
             (random.uniform(-10, 10), random.uniform(-10, 10)), (random.uniform(-10, 10), random.uniform(-10, 10))]
start_point = (-10, random.uniform(-10, 10))
finish_point = (10, random.uniform(-10, 10))

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)


kp = 1
koi = 1
d0 = 40

# qr    wspolrzedne polozenia robota
# qk    wspolrzedne celu
# pk    wspolczynnik dobierany doswiadczalnie
# qoi   wspolrzedne polozenia i-tej przeszkody
# koi   wspolczynnik dla kazdej przeszkody dobierany doswiadczalnie
# d0    odleglosc graniczna oddzialywania przeszkody


# potencjal przyciagajacy
def Up(qr, qk, kp):
    result = 1/2 * kp * np.linalg.norm(np.subtract(qr, qk))**2
    return result


# potencjal odpychajacy od i-tej przeszkody
def Voi(koi, qr, qoi, d0):
    condition = np.linalg.norm(np.subtract(qr, qoi))
    if condition <= d0:
        result = 1/2 * koi * ((1/condition) - (1/d0))**2
        return result
    else:
        return 0


# potencjal wypadkowy
def Uw(qr, qk, kp, koi, d0, obstacles):
    return Up(qr, qk, kp) + sum([Voi(koi, qr, qoi, d0) for qoi in obstacles])


# sila?
def Fp(qr, qk, kp):
    result = kp * np.linalg.norm(np.subtract(qr, qk))
    return result


def Foi(koi, qr, qoi, d0):
    condition = np.linalg.norm(np.subtract(qr, qoi))
    if condition <= d0 and condition != 0:
        result = -koi * ((1/condition)-(1/d0)) * (1 / condition**2)
        return result
    else:
        return 0


def F(qr, qk, kp, koi, d0, obstacles):
    return Fp(qr, qk, kp) - sum([Foi(koi, qr, qoi, d0) for qoi in obstacles])


# zad 1a
# for i in range(len(x)):
#     for j in range(len(y)):
#         # print(_x,_y)
#         Z[j][i] = F((x[i], y[j]), finish_point, kp, koi, d0, obst_vect)
#         if Z[j][i] < -1:
#             Z[j][i] = -1


# wyznaczanie sciezki
path = []
index_x = 0
index_y = 0

while np.abs(y[index_y]-start_point[1]) > delta:
    index_y += 1

# while np.linalg.norm((x[index_x]-finish_point[0], y[index_y]-finish_point[1])) > 2*delta:

#     path.append((x[index_x], y[index_y]))

#     up = 1 if index_y != len(y)-1 else 0
#     down = 1 if index_y != 0 else 0
#     right = 1 if index_x != len(x)-1 else 0
#     left = 1 if index_x != 0 else 0

#     direction = {
#         'Up': Z[index_y+up][index_x],
#         'Down': Z[index_y-down][index_x],
#         'Right': Z[index_y][index_x+right],
#         'Left': Z[index_y][index_x-left],
#         'UpRight': Z[index_y+up][index_x+right],
#         'DownRight': Z[index_y-down][index_x+right],
#         'UpLeft': Z[index_y+up][index_x-left],
#         'DownLeft': Z[index_y-down][index_x-left]
#     }

#     move = min(direction, key=direction.get)

#     if move == 'Up':
#         index_y += up
#     elif move == 'Down':
#         index_y -= down
#     elif move == 'Right':
#         index_x += right
#     elif move == 'Left':
#         index_x -= left
#     elif move == 'UpRight':
#         index_y += up
#         index_x += right
#     elif move == 'DownRight':
#         index_y -= down
#         index_x += right
#     elif move == 'UpLeft':
#         index_y += up
#         index_x -= left
#     elif move == 'DownLeft':
#         index_y -= down
#         index_x -= left


while np.linalg.norm((x[index_x]-finish_point[0], y[index_y]-finish_point[1])) > 2*delta:

    path.append((x[index_x], y[index_y]))

    up = 1 if index_y != len(y)-1 else 0
    down = 1 if index_y != 0 else 0
    right = 1 if index_x != len(x)-1 else 0
    left = 1 if index_x != 0 else 0

    # Z[index_y][index_x] = F((x[index_x], y[index_y]),
                            #    finish_point, kp, koi, d0, obst_vect)

    Z[index_y+up][index_x] = F((x[index_x], y[index_y+up]),
                               finish_point, kp, koi, d0, obst_vect)
    Z[index_y-down][index_x] = F((x[index_x], y[index_y-down]),
                               finish_point, kp, koi, d0, obst_vect)
    Z[index_y][index_x+right] = F((x[index_x+right], y[index_y]),
                               finish_point, kp, koi, d0, obst_vect)
    Z[index_y][index_x-left] = F((x[index_x-left], y[index_y]),
                               finish_point, kp, koi, d0, obst_vect)
    Z[index_y+up][index_x+right] = F((x[index_x+right], y[index_y+up]),
                               finish_point, kp, koi, d0, obst_vect)
    Z[index_y-down][index_x+right] = F((x[index_x+right], y[index_y-down]),
                               finish_point, kp, koi, d0, obst_vect)
    Z[index_y+up][index_x-left] = F((x[index_x-left], y[index_y+up]),
                               finish_point, kp, koi, d0, obst_vect)
    Z[index_y-down][index_x-left] = F((x[index_x-left], y[index_y-down]),
                               finish_point, kp, koi, d0, obst_vect)

    direction = {
        'Up': Z[index_y+up][index_x],
        'Down': Z[index_y-down][index_x],
        'Right': Z[index_y][index_x+right],
        'Left': Z[index_y][index_x-left],
        'UpRight': Z[index_y+up][index_x+right],
        'DownRight': Z[index_y-down][index_x+right],
        'UpLeft': Z[index_y+up][index_x-left],
        'DownLeft': Z[index_y-down][index_x-left]
    }

    move = min(direction, key=direction.get)

    if move == 'Up':
        index_y += up
    elif move == 'Down':
        index_y -= down
    elif move == 'Right':
        index_x += right
    elif move == 'Left':
        index_x -= left
    elif move == 'UpRight':
        index_y += up
        index_x += right
    elif move == 'DownRight':
        index_y -= down
        index_x += right
    elif move == 'UpLeft':
        index_y += up
        index_x -= left
    elif move == 'DownLeft':
        index_y -= down
        index_x -= left



fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmax=20, vmin=0)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')


# for step in path:
    # plt.plot(step[0], step[1], "*", color='yellow')


graph, = plt.plot([], [], 'or')

def animate(i):
    graph.set_data(path[i][0], path[i][1])
    plt.plot(path[i][0], path[i][1], "*", color='yellow')
    return graph


ani = FuncAnimation(fig, animate, frames=250, interval=50)


plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
